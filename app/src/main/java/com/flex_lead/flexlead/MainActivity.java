package com.flex_lead.flexlead;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.flex_lead.flexlead.Service.DeliveryServiceFragment;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback {

    private static final String TAG = "Map: ";
    private GoogleMap mMap;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 99;
    private static final int DEFAULT_ZOOM = 18;
    private final LatLng mDefaultLocation = new LatLng(23.777176, 90.399452);
    private LocationManager locationManager;
    private boolean mLocationPermissionGranted;
    private FusedLocationProviderClient mFusedLocationProviderClient;
    private Location mLastKnownLocation;
    private PlaceAutocompleteFragment placeAutocompleteFragmentForSearch;
    private FloatingActionButton fab;

    public Marker startMarker, endMarker;

    private TextView searchHeader;
    private TextView searchFooter;

    private static final int animationDuration = 700;
    private static final int animationRepeat = 0;

    private BottomSheetDialogFragment deliveryServiceBottomSheetDialogFragment;
    private BottomSheetDialogFragment loginBottomSheetDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        bindViews();

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        placeAutocompleteFragmentForSearch = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete);

        EditText search = Objects.requireNonNull(placeAutocompleteFragmentForSearch.getView()).findViewById(R.id.place_autocomplete_search_input);
        ImageView searchIcon = (ImageView) ((LinearLayout) placeAutocompleteFragmentForSearch.getView()).getChildAt(0);

        search.setHintTextColor(getResources().getColor(R.color.colorWhite));
        search.setTextColor(getResources().getColor(R.color.colorWhite));

        searchIcon.setImageDrawable(getResources().getDrawable(R.drawable.ic_location_searching_24dp));

        placeAutocompleteFragmentForSearch.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                moveCamera(place);

            }

            @Override
            public void onError(Status status) {
                Log.d("Maps", "An error occurred: " + status);
            }
        });

        loginBottomSheetDialogFragment.show(getSupportFragmentManager(), loginBottomSheetDialogFragment.getTag());

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        searchHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                placeAutocompleteFragmentForSearch.getView().setVisibility(View.VISIBLE);
                destroyAllClickListener();
                selectStartLocation();

            }
        });

        searchFooter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                placeAutocompleteFragmentForSearch.getView().setVisibility(View.VISIBLE);
                destroyAllClickListener();
                selectEndLocation();

            }
        });

    }

    private void bindViews() {

        searchHeader = findViewById(R.id.search_header);
        searchFooter = findViewById(R.id.search_footer);

        searchHeader.setVisibility(View.GONE);
        searchFooter.setVisibility(View.GONE);

        deliveryServiceBottomSheetDialogFragment = new DeliveryServiceFragment();
        loginBottomSheetDialogFragment = new LoginFragment();
        loginBottomSheetDialogFragment.setCancelable(false);

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(mDefaultLocation));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM));

        // Prompt the user for permission.
        getLocationPermission();

        // Turn on the My Location layer and the related control on the map.
        updateLocationUI();

        // Get the current location of the device and set the position of the map.
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            getDeviceLocation();

        } else {

            turnGPSOn();

        }

    }

    public void turnGPSOn() {

        new StartLocationAlert(MainActivity.this);

        /*final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        *//*startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));*//*



                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();*/

    }

    public void moveCamera(Place place) {

        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position(place.getLatLng());
        markerOptions.title(place.getName() + "");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM));

    }

    private Marker addMarker(LatLng latLng) {

        MarkerOptions markerOptions = new MarkerOptions();

        markerOptions.position(latLng);
        markerOptions.title("");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));

        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(DEFAULT_ZOOM));

//        locationButtonEnable(false);

        fab.show();

        return mMap.addMarker(markerOptions);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_service_delivery) {


            resetEverything();
            selectStartLocation();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * Prompts the user for permission to use the device location.
     */
    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    private void updateLocationUI() {
        if (mMap == null) {
            return;
        }
        try {
            if (mLocationPermissionGranted) {
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                repositionLocationButton();
            } else {
                mMap.setMyLocationEnabled(false);
                mMap.getUiSettings().setMyLocationButtonEnabled(false);
                mLastKnownLocation = null;
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void repositionLocationButton() {

        View locationButton = ((View) findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

        // and next place it, for exemple, on bottom right (as Google Maps app)
        RelativeLayout.LayoutParams rlp = (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
        // position on right bottom
        rlp.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        rlp.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        rlp.setMargins(30, 0, 0, 30);

        //getDeviceLocation();

    }

    /**
     * Gets the current location of the device, and positions the map's camera.
     */
    private void getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (mLocationPermissionGranted) {
                Task<Location> locationResult = mFusedLocationProviderClient.getLastLocation();
                locationResult.addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful()) {
                            // Set the map's camera position to the current location of the device.
                            mLastKnownLocation = task.getResult();
                            if (mLastKnownLocation != null) {
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mLastKnownLocation.getLatitude(),
                                                mLastKnownLocation.getLongitude()), DEFAULT_ZOOM));
                            }
                        } else {
                            Log.d(TAG, "Current location is null. Using defaults.");
                            Log.e(TAG, "Exception: %s", task.getException());
                            mMap.moveCamera(CameraUpdateFactory
                                    .newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                            mMap.getUiSettings().setMyLocationButtonEnabled(false);
                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.e("Exception: %s", e.getMessage());
        }
    }

    private void selectStartLocation() {

        destroyAllClickListener();

        if (endMarker == null)
            searchFooter.setVisibility(View.GONE);

        placeAutocompleteFragmentForSearch.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                addStartMarker(place.getLatLng());

            }

            @Override
            public void onError(Status status) {

            }
        });

        searchHeader.setVisibility(View.VISIBLE);
        YoYo.with(Techniques.FadeIn)
                .duration(animationDuration)
                .repeat(animationRepeat)
                .playOn(searchHeader);

        searchHeader.setText(getResources().getString(R.string.start_location));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                addStartMarker(latLng);
                geoLocationFromLatLng(latLng);

            }
        });

        Objects.requireNonNull(placeAutocompleteFragmentForSearch.getView()).findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearScreen(startMarker, View.GONE, true, v);

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchHeader.setText(String.format("%s: %s", getResources().getString(R.string.start_location), geoLocationFromLatLng(startMarker.getPosition())));
                placeAutocompleteFragmentForSearch.setText("");
                if (endMarker == null) {

                    fab.hide();
                    destroyAllClickListener();
                    selectEndLocation();

                } else {
                    placeAutocompleteFragmentForSearch.getView().setVisibility(View.GONE);
                    deliveryServiceBottomSheetDialogFragment.show(getSupportFragmentManager(), deliveryServiceBottomSheetDialogFragment.getTag());
                    fab.hide();
                    destroyAllClickListener();


                }

            }
        });

    }

    private void selectEndLocation() {

        destroyAllClickListener();

        placeAutocompleteFragmentForSearch.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {

                addEndMarker(place.getLatLng());

            }

            @Override
            public void onError(Status status) {

            }
        });

        searchFooter.setVisibility(View.VISIBLE);

        YoYo.with(Techniques.FadeIn)
                .duration(animationDuration)
                .repeat(animationRepeat)
                .playOn(searchFooter);

        searchFooter.setText(getResources().getString(R.string.end_location));

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {

                addEndMarker(latLng);
                geoLocationFromLatLng(latLng);

            }
        });

        Objects.requireNonNull(placeAutocompleteFragmentForSearch.getView()).findViewById(R.id.place_autocomplete_clear_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clearScreen(endMarker, View.GONE, true, v);

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                searchFooter.setText(String.format("%s: %s", getResources().getString(R.string.end_location), geoLocationFromLatLng(endMarker.getPosition())));
                placeAutocompleteFragmentForSearch.getView().setVisibility(View.GONE);
                deliveryServiceBottomSheetDialogFragment.show(getSupportFragmentManager(), deliveryServiceBottomSheetDialogFragment.getTag());
                fab.hide();
                destroyAllClickListener();


            }
        });

    }

    private void destroyAllClickListener() {

        fab.setOnClickListener(null);
        placeAutocompleteFragmentForSearch.setOnPlaceSelectedListener(null);

    }

    private void addStartMarker(LatLng latLng) {

        if (startMarker == null) {

            startMarker = addMarker(latLng);

        } else {
            startMarker.remove();
            startMarker = addMarker(latLng);
        }

    }

    private void addEndMarker(LatLng latLng) {

        if (endMarker == null) {

            endMarker = addMarker(latLng);

        } else {
            endMarker.remove();
            endMarker = addMarker(latLng);
        }

    }

    private void clearScreen(Marker marker, int visibility, boolean state, View view) {

        placeAutocompleteFragmentForSearch.setText("");
        view.setVisibility(visibility);
        if (state)
            fab.show();
        else
            fab.hide();
        locationButtonEnable(state);
        if (marker != null)
            marker.remove();

    }

    private void locationButtonEnable(boolean state) {

        if (ContextCompat.checkSelfPermission(MainActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(state);

        }

    }

    private String geoLocationFromLatLng(LatLng latLng) {

        Geocoder gcd = new Geocoder(MainActivity.this, Locale.getDefault());
        List<Address> addresses = null;
        try {

            addresses = gcd.getFromLocation(latLng.latitude, latLng.longitude, 1);
            if (addresses.size() > 0) {
                placeAutocompleteFragmentForSearch.setText(addresses.get(0).getAddressLine(0));
                return addresses.get(0).getAddressLine(0);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";

    }

    private void resetEverything() {

        destroyAllClickListener();
        placeAutocompleteFragmentForSearch.setText("");
        fab.hide();
        locationButtonEnable(true);
        if (startMarker != null)
            startMarker.remove();

        if (endMarker != null)
            endMarker.remove();

        startMarker = null;
        endMarker = null;

        Objects.requireNonNull(placeAutocompleteFragmentForSearch.getView()).setVisibility(View.VISIBLE);

        YoYo.with(Techniques.FadeIn)
                .duration(animationDuration)
                .repeat(animationRepeat)
                .playOn(placeAutocompleteFragmentForSearch.getView());

        searchFooter.setVisibility(View.GONE);

    }

    public void serviceBottomDialogDismiss() {

        fab.show();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deliveryServiceBottomSheetDialogFragment.show(getSupportFragmentManager(), deliveryServiceBottomSheetDialogFragment.getTag());
                fab.setOnClickListener(null);
                fab.hide();

            }
        });

    }

}
