package com.flex_lead.flexlead.Service;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.flex_lead.flexlead.API;
import com.flex_lead.flexlead.Config;
import com.flex_lead.flexlead.MainActivity;
import com.flex_lead.flexlead.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DeliveryServiceFragment extends BottomSheetDialogFragment {

    private static final String TAG = "Delivery Service";
    @BindView(R.id.confirm)
    Button confirmButton;
    @BindView(R.id.receiver_name)
    TextInputEditText receiverName;
    @BindView(R.id.receiver_phone)
    TextInputEditText receiverPhone;
    @BindView(R.id.receiver_name_edit_text_input_layout)
    TextInputLayout receiverNameHolder;
    @BindView(R.id.receiver_phone_edit_text_input_layout)
    TextInputLayout receiverPhoneHolder;
    private SharedPreferences sharedPreferences;
    private String receiverNameSaveKey = "receiverNameSaveKey", receiverPhoneSaveKey = "receiverPhoneSaveKey";
    private BottomSheetBehavior<View> bottomSheetBehavior;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {

                bottomSheetBehavior.setPeekHeight(Config.bottomSheetDefaultPeekHeight);
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);

            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);

        if (getActivity() != null)
            ((MainActivity) getActivity()).serviceBottomDialogDismiss();

    }

    @SuppressLint("RestrictedApi")
    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);

        View contentView = View.inflate(getContext(), R.layout.service_delivery_bottomsheet, null);
        ButterKnife.bind(this, contentView);
        dialog.setContentView(contentView);

        /*if (getActivity() != null)
            sharedPreferences = getActivity().getSharedPreferences(Config.SP_FlexLead_APP, MODE_PRIVATE);*/

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();
        ((View) contentView.getParent()).setBackgroundColor(getResources().getColor(R.color.bottomSheetBackground));

        if (behavior != null && behavior instanceof BottomSheetBehavior) {

            bottomSheetBehavior = (BottomSheetBehavior<View>) behavior;
            ((BottomSheetBehavior) behavior).setState(BottomSheetBehavior.STATE_EXPANDED);
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);

        }
    }

    @OnClick({R.id.confirm})
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.confirm: {

                if (receiverName.getText().toString().equals("") && receiverPhone.getText().toString().equals("")) {

                    if (receiverName.getText().toString().equals(""))
                        receiverName.setError("Empty");

                    if (receiverPhone.getText().toString().equals("")) {
                        receiverPhone.setError("Empty");
                    }

                } else {

                    String sLat, sLong, eLat, eLong;

                    sLat = Double.toString(((MainActivity) getActivity()).startMarker.getPosition().latitude);
                    sLong = Double.toString(((MainActivity) getActivity()).startMarker.getPosition().longitude);
                    eLat = Double.toString(((MainActivity) getActivity()).endMarker.getPosition().latitude);
                    eLong = Double.toString(((MainActivity) getActivity()).endMarker.getPosition().longitude);

                    deliveryRequest(sLat, sLong, eLat, eLong);
                }



                break;
            }

        }

    }

    /*@Override
    public void onPause() {
        super.onPause();

        SharedPreferences.Editor editor = sharedPreferences.edit();

        editor.putString(receiverNameSaveKey, receiverName.getText().toString());
        editor.putString(receiverPhoneSaveKey, receiverPhone.getText().toString());

        editor.apply();

    }

    @Override
    public void onResume() {
        super.onResume();

        if (getActivity() != null) {

            receiverName.setText(getActivity().getSharedPreferences(Config.SP_FlexLead_APP, MODE_PRIVATE).getString(receiverNameSaveKey, ""));
            receiverPhone.setText(getActivity().getSharedPreferences(Config.SP_FlexLead_APP, MODE_PRIVATE).getString(receiverPhoneSaveKey, ""));
        }

    }*/

    private void deliveryRequest(final String sLat, final String sLong, final String eLat, final String eLong) {

        if (getActivity() != null) {

            // Instantiate the RequestQueue.
            RequestQueue queue = Volley.newRequestQueue(getActivity());
            String url = API.baseUrl + API.deliverRequest;

            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                    new Response.Listener<String>() {
                        @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                        @Override
                        public void onResponse(String response) {

                            //Log.v("Login response from server ", " " + response);

                            try {

                                JSONObject jsonObject = new JSONObject(response);
                                String code = jsonObject.getString(API.key_code);

                                if (code.equals(API.VALUE_CODE_SUCCESS)) {

                                    dismiss();

                                /*progressBar.setVisibility(View.GONE);
                                userInfo(jsonObject);*/

                                } else {

                                /*loginProgressBar.setVisibility(View.GONE);
                                loginBtn.setVisibility(View.VISIBLE);
                                Toast.makeText(getBaseContext(), "Invalid Email or password", Toast.LENGTH_LONG).show();
                                progressBar.setVisibility(View.GONE);
                                loginBtn.setVisibility(View.VISIBLE);*/

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            /*progressBar.setVisibility(View.GONE);
                            loginBtn.setVisibility(View.VISIBLE);*/
                            }

                            /* */

                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {


                /*progressBar.setVisibility(View.GONE);
                loginBtn.setVisibility(View.VISIBLE);*/

                    try {

                    /*if (error.getMessage().equals("com.android.volley.TimeoutError"))
                        Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_timeout), Toast.LENGTH_SHORT).show();

                    Toast.makeText(LoginActivity.this, getResources().getString(R.string.server_unreachable), Toast.LENGTH_SHORT).show();*/


                        Log.v("Volly error : ", "" + error);

                    } catch (Exception e) {

                        Log.v("Distribute Cancel E : ", " " + e);

                    }


                }

            }) {

                /*@Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put(API.KEY_CONTENT_TYPE, API.VALUE_CONTENT_VALUE);

                    return params;
                }*/

                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put(API.keyClientId, "1");
                    params.put(API.keyReceiverName, receiverName.getText().toString());
                    params.put(API.keyReceiverPhone, receiverPhone.getText().toString());
                    params.put(API.keyGeoStartLatitude, sLat);
                    params.put(API.keyGeoStartLongitude, sLong);
                    params.put(API.keyGeoEndLatitude, eLat);
                    params.put(API.keyGeoEndLongitude, eLong);
                    params.put(API.keyWeight, "10-15");
                    return params;
                }
            };


            // Add the request to the RequestQueue.
            queue.add(stringRequest);

        }

    }

}
